import React, { Component } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";

function Pergunta(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/logo.png")}
        resizeMode="contain"
        style={styles.image1}
      ></Image>
      <View style={styles.container3}>
        <Text style={styles.textoPergunta}>
          1 - No exemplo abaixo, temos uma lista de animais. Remova qual animal
          não é um mamífero:
        </Text>
        <View style={styles.containerPergunta}>
          <Text style={styles.textoCodigo}>
            const mamiferos = [&#39;Leão&#39;, &#39;Urso&#39;,
            &#39;cachorro&#39;];{"\n"}
            {"\n"}mamiferos.filter(mamifero =&gt; mamifero !== (?) );
          </Text>
        </View>
        <View style={styles.alternativaARow}>
          <View style={styles.alternativaA}>
            <Text style={styles.textoAlternativaA}>A</Text>
          </View>
          <Text style={styles.respostaA}>Leão</Text>
        </View>
        <View style={styles.buttonRow}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("MensagemSucesso")}
            style={styles.button}
          >
            <Text style={styles.textoAlternativaB}>B</Text>
          </TouchableOpacity>
          <Text style={styles.respostaB}>Urso</Text>
        </View>
        <View style={styles.alternativaCRow}>
          <View style={styles.alternativaC}>
            <Text style={styles.textoAlternativaC}>C</Text>
          </View>
          <Text style={styles.respostaC}>Cachorro</Text>
        </View>
        <View style={styles.alternativaDRow}>
          <View style={styles.alternativaD}>
            <Text style={styles.textoAlternativaD}>D</Text>
          </View>
          <Text style={styles.respostaD}>urso</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image1: {
    width: 78,
    height: 39,
    marginTop: 52,
    alignSelf: "center"
  },
  container3: {
    width: 375,
    height: 552,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 52
  },
  textoPergunta: {
    width: 311,
    height: 54,
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginTop: 10,
    marginLeft: 36
  },
  containerPergunta: {
    width: 302,
    height: 79,
    backgroundColor: "rgba(230, 230, 230,1)",
    marginTop: 12,
    marginLeft: 36
  },
  textoCodigo: {
    width: 267,
    height: 40,
    color: "rgba(53,52,52,1)",
    fontSize: 11,
    fontFamily: "roboto-700",
    marginTop: 22,
    marginLeft: 18
  },
  alternativaA: {
    width: 37,
    height: 32,
    backgroundColor: "rgba(12,12,12,1)",
    borderRadius: 5
  },
  textoAlternativaA: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 9,
    marginLeft: 13
  },
  respostaA: {
    width: 47,
    height: 17,
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 15,
    marginTop: 15
  },
  alternativaARow: {
    height: 32,
    flexDirection: "row",
    marginTop: 16,
    marginLeft: 36,
    marginRight: 240
  },
  button: {
    width: 37,
    height: 32,
    backgroundColor: "rgba(255,182,6,1)",
    borderRadius: 5
  },
  textoAlternativaB: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 9,
    marginLeft: 13
  },
  respostaB: {
    width: 47,
    height: 17,
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 15,
    marginTop: 18
  },
  buttonRow: {
    height: 35,
    flexDirection: "row",
    marginTop: 10,
    marginLeft: 36,
    marginRight: 240
  },
  alternativaC: {
    width: 37,
    height: 32,
    backgroundColor: "rgba(12,12,12,1)",
    borderRadius: 5
  },
  textoAlternativaC: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 8,
    marginLeft: 13
  },
  respostaC: {
    width: 72,
    height: 17,
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 15,
    marginTop: 19
  },
  alternativaCRow: {
    height: 36,
    flexDirection: "row",
    marginTop: 9,
    marginLeft: 36,
    marginRight: 215
  },
  alternativaD: {
    width: 37,
    height: 32,
    backgroundColor: "rgba(12,12,12,1)",
    borderRadius: 5
  },
  textoAlternativaD: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 9,
    marginLeft: 13
  },
  respostaD: {
    width: 72,
    height: 17,
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 15,
    marginTop: 21
  },
  alternativaDRow: {
    height: 38,
    flexDirection: "row",
    marginTop: 7,
    marginLeft: 36,
    marginRight: 215
  }
});

export default Pergunta;
