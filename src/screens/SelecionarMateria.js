import React, { Component } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";

function SelecionarMateria(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/logo.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
      <View style={styles.container2}>
        <Text style={styles.titulo}>Qual sua matéria favorita?</Text>
        <View style={styles.buttonRow}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Pergunta")}
            style={styles.button}
          >
            <Text style={styles.a2}>A</Text>
          </TouchableOpacity>
          <Text style={styles.matematica}>Matemática</Text>
        </View>
        <View style={styles.button4Row}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Pergunta")}
            style={styles.button4}
          >
            <Text style={styles.b2}>B</Text>
          </TouchableOpacity>
          <Text style={styles.geografia}>Geografia</Text>
        </View>
        <View style={styles.button3Row}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Pergunta")}
            style={styles.button3}
          >
            <Text style={styles.c8}>C</Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Pergunta")}
              style={styles.button5}
            >
              <Text style={styles.d}>D</Text>
            </TouchableOpacity>
          </TouchableOpacity>
          <Text style={styles.historia}>História</Text>
        </View>
        <Text style={styles.biologia}>Biologia</Text>
        <View style={styles.button2Row}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Pergunta")}
            style={styles.button2}
          >
            <Text style={styles.e8}>E</Text>
          </TouchableOpacity>
          <Text style={styles.fisica}>Física</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 78,
    height: 39,
    marginTop: 52,
    alignSelf: "center"
  },
  container2: {
    width: 375,
    height: 485,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 48
  },
  titulo: {
    width: 260,
    height: 31,
    color: "rgba(53,52,52,1)",
    fontSize: 22,
    fontFamily: "roboto-300",
    marginTop: 12,
    marginLeft: 57
  },
  button: {
    width: 32,
    height: 29,
    backgroundColor: "rgba(255,182,6,1)",
    borderRadius: 5
  },
  a2: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 8,
    marginLeft: 11
  },
  matematica: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 11,
    marginTop: 7
  },
  buttonRow: {
    height: 29,
    flexDirection: "row",
    marginTop: 17,
    marginLeft: 59,
    marginRight: 188
  },
  button4: {
    width: 32,
    height: 29,
    backgroundColor: "rgba(255,182,6,1)",
    borderRadius: 5
  },
  b2: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 7,
    marginLeft: 11
  },
  geografia: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 11,
    marginTop: 7
  },
  button4Row: {
    height: 29,
    flexDirection: "row",
    marginTop: 18,
    marginLeft: 59,
    marginRight: 204
  },
  button3: {
    width: 32,
    height: 29,
    backgroundColor: "rgba(255,182,6,1)",
    borderRadius: 5
  },
  c8: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 7,
    marginLeft: 11
  },
  button5: {
    width: 32,
    height: 29,
    backgroundColor: "rgba(255,182,6,1)",
    borderRadius: 5,
    marginTop: 22
  },
  d: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 7,
    marginLeft: 11
  },
  historia: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 11,
    marginTop: 7
  },
  button3Row: {
    height: 29,
    flexDirection: "row",
    marginTop: 16,
    marginLeft: 59,
    marginRight: 217
  },
  biologia: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginTop: 23,
    marginLeft: 102
  },
  button2: {
    width: 32,
    height: 29,
    backgroundColor: "rgba(255,182,6,1)",
    borderRadius: 5
  },
  e8: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-700",
    marginTop: 8,
    marginLeft: 11
  },
  fisica: {
    color: "rgba(53,52,52,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 11,
    marginTop: 7
  },
  button2Row: {
    height: 29,
    flexDirection: "row",
    marginTop: 22,
    marginLeft: 59,
    marginRight: 231
  }
});

export default SelecionarMateria;
