import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import MaterialButtonViolet from "../components/MaterialButtonViolet";

function MeuSaldo(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/logo.png")}
        resizeMode="contain"
        style={styles.image2}
      ></Image>
      <View style={styles.container1}>
        <Text style={styles.text}>Meu Saldo</Text>
        <Text style={styles.valor}>R$ 150,00</Text>
        <View style={styles.jogarStack}>
          <MaterialButtonViolet
            container="SelecionarMateria"
            style={styles.jogar}
          ></MaterialButtonViolet>
          <Image
            source={require("../assets/images/play1.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image2: {
    width: 117,
    height: 65,
    marginTop: 198,
    marginLeft: 129
  },
  container1: {
    width: 315,
    height: 171,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 22,
    marginLeft: 30
  },
  text: {
    width: 109,
    height: 31,
    color: "rgba(53,52,52,1)",
    fontSize: 22,
    fontFamily: "roboto-300",
    marginTop: 14,
    alignSelf: "center"
  },
  valor: {
    width: 232,
    height: 51,
    color: "rgba(53,52,52,1)",
    fontSize: 50,
    fontFamily: "roboto-700",
    marginTop: 15,
    alignSelf: "center"
  },
  jogar: {
    width: 226,
    height: 36,
    backgroundColor: "rgba(255,182,6,1)",
    position: "absolute",
    bottom: 0,
    left: 0
  },
  image: {
    top: 6,
    left: 149,
    width: 22,
    height: 22,
    position: "absolute"
  },
  jogarStack: {
    width: 226,
    height: 36,
    marginTop: 24,
    marginLeft: 45
  }
});

export default MeuSaldo;
