import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import MaterialButtonViolet3 from "../components/MaterialButtonViolet3";
import MaterialButtonViolet1 from "../components/MaterialButtonViolet1";

function MensagemSucesso(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/logo.png")}
        resizeMode="contain"
        style={styles.image1}
      ></Image>
      <View style={styles.container4}>
        <Text style={styles.ebaaaaa}>Ebaaaaa!</Text>
        <View style={styles.respostaCorretaStack}>
          <Text style={styles.respostaCorreta}>Resposta Correta!</Text>
          <Image
            source={require("../assets/images/star.png")}
            resizeMode="contain"
            style={styles.star}
          ></Image>
        </View>
        <Text style={styles.voceGanhou}>Você ganhou</Text>
        <Text style={styles.r500}>R$ 5,00</Text>
        <View style={styles.proximaStack}>
          <MaterialButtonViolet3
            caption=""
            container="Pergunta"
            style={styles.proxima}
          ></MaterialButtonViolet3>
          <Image
            source={require("../assets/images/play11.png")}
            resizeMode="contain"
            style={styles.play}
          ></Image>
        </View>
        <MaterialButtonViolet1
          container="MeuSaldo"
          style={styles.terminar}
        ></MaterialButtonViolet1>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image1: {
    width: 78,
    height: 39,
    marginTop: 52,
    alignSelf: "center"
  },
  container4: {
    width: 375,
    height: 390,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 44
  },
  ebaaaaa: {
    width: 79,
    height: 16,
    color: "rgba(53,52,52,1)",
    fontSize: 18,
    fontFamily: "roboto-300",
    marginTop: 23,
    alignSelf: "center"
  },
  respostaCorreta: {
    top: 0,
    width: 178,
    height: 26,
    color: "rgba(53,52,52,1)",
    position: "absolute",
    fontSize: 22,
    fontFamily: "roboto-regular",
    left: 0
  },
  star: {
    top: 22,
    width: 80,
    height: 80,
    position: "absolute",
    left: 49
  },
  respostaCorretaStack: {
    width: 178,
    height: 102,
    marginTop: 19,
    marginLeft: 99
  },
  voceGanhou: {
    width: 126,
    height: 18,
    color: "rgba(53,52,52,1)",
    fontSize: 22,
    fontFamily: "roboto-regular",
    marginTop: 24,
    marginLeft: 125
  },
  r500: {
    width: 180,
    height: 23,
    color: "rgba(53,52,52,1)",
    fontSize: 50,
    fontFamily: "roboto-700",
    marginTop: 13,
    marginLeft: 98
  },
  proxima: {
    top: 0,
    left: 0,
    width: 231,
    height: 36,
    position: "absolute"
  },
  play: {
    top: 4,
    left: 145,
    width: 24,
    height: 24,
    position: "absolute"
  },
  proximaStack: {
    width: 231,
    height: 36,
    marginTop: 43,
    marginLeft: 72
  },
  terminar: {
    width: 229,
    height: 33,
    marginTop: 12,
    marginLeft: 73
  }
});

export default MensagemSucesso;
