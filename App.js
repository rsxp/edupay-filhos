import React, { useState } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import MensagemSucesso from "./src/screens/MensagemSucesso";
import MeuSaldo from "./src/screens/MeuSaldo";
import Pergunta from "./src/screens/Pergunta";
import SelecionarMateria from "./src/screens/SelecionarMateria";

const DrawerNavigation = createDrawerNavigator({
  MensagemSucesso: MensagemSucesso,
  MeuSaldo: MeuSaldo,
  Pergunta: Pergunta,
  SelecionarMateria: SelecionarMateria
});

const StackNavigation = createStackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
    MensagemSucesso: MensagemSucesso,
    MeuSaldo: MeuSaldo,
    Pergunta: Pergunta,
    SelecionarMateria: SelecionarMateria
  },
  {
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(StackNavigation);

function App() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return isLoadingComplete ? <AppContainer /> : <AppLoading />;
  }
}
async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      "roboto-700": require("./src/assets/fonts/roboto-700.ttf"),
      "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf"),
      "roboto-300": require("./src/assets/fonts/roboto-300.ttf")
    })
  ]);
}
function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;
